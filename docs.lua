-- Таблицы
-- плохо
local player = {}
player.name = 'Jack'
player.class = 'Rogue'

-- хорошо
local player = {
  name = 'Jack',
  class = 'Rogue'
}

-- Nil не считаются элементами массива
local list = {}
list[0] = nil
list[1] = 'item'

print(#list) -- 0
print(select('#', list)) -- 1

-- Для строк необходимо использовать одинарные ковычки
-- плохо
local name = "Bob Parr"

-- хорошо
local name = 'Bob Parr'

-- плохо
local fullName = "Bob " .. self.lastName

-- хорошо
local fullName = 'Bob ' .. self.lastName

-- Строки больше 80 символов необходимо писать в разных строках
-- плохо
local errorMessage = 'This is a super long error that was thrown because of Batman. When you stop to think about how Batman had anything to do with this, you would get nowhere fast.'

-- хорошо
local errorMessage = 'This is a super long error that \
was thrown because of Batman. \
When you stop to think about \
how Batman had anything to do \
with this, you would get nowhere \
fast.'

-- плохо
local errorMessage = [[This is a super long error that
  was thrown because of Batman.
  When you stop to think about
  how Batman had anything to do
  with this, you would get nowhere
  fast.]]

-- хорошо
local errorMessage = 'This is a super long error that ' ..
  'was thrown because of Batman. ' ..
  'When you stop to think about ' ..
  'how Batman had anything to do ' ..
  'with this, you would get nowhere ' ..
  'fast.'


local luke = {
  jedi = true,
  age = 28
}

-- плохо
local isJedi = luke['jedi']

-- хорошо
local isJedi = luke.jedi

-- Все переменные необходимо указывать наверху функции
-- Плохо
local bad = function()
  test()
  print('doing stuff..')

  --..other stuff..

  local name = getName()

  if name == 'test' then
    return false
  end

  return name
end

-- Хорошо
local CCX = display.contentCenterX

local function good()
  local name = getName()

  test()
  print('doing stuff..')

  --..other stuff..

  if name == 'test' then
    return false
  end

  return name
end

-- Используй сокращения

-- Плохо
if name ~= nil then
  -- ...stuff...
end

-- Хорошо
if name then
  -- ...stuff...
end

-- Лучше использовать конструкцию else, где это имеет смысл
-- Плохо
local function full_name(first, last)
  local name

  if first and last then
    name = first .. ' ' .. last
  else
    name = 'John Smith'
  end

  return name
end

-- Хорошо
local function full_name(first, last)
  local name = 'John Smith'

  if first and last then
    name = first .. ' ' .. last
  end

  return name
end


-- Необходимо использовать 2 пробела для отступов
-- Плохо
function() 
∙∙∙∙local name
end

-- Хорошо
function() 
∙local name
end

-- Плохо
function() 
∙∙local name
end

-- Нужно ставить 1 пробел перед открывающими и закрывающими скобками
-- Плохо
local test = {one=1}

-- Хорошо
local test = { one = 1 }

-- Плохо
dog.set('attr',{
  age = '1 year',
  breed = 'Bernese Mountain Dog'
})

-- Хорошо
dog.set('attr', {
  age = '1 year',
  breed = 'Bernese Mountain Dog'
})

-- Окружить операторы пробелами
-- Плохо
local thing=1
thing = thing-1
thing = thing*1
thing = 'string'..'s'

-- Хорошо
local thing = 1
thing = thing - 1
thing = thing * 1
thing = 'string' .. 's'

--Использовать один пробел после запятых
-- Плохо
local thing = {1,2,3}
thing = {1 , 2 , 3}
thing = {1 ,2 ,3}

-- Хорошо
local thing = {1, 2, 3}
local data = {
  1, 2, 3;
  4, 5, 6;
  7, 8, 9;
}

-- Необходима пустая строка после мультистроковых блоков
-- Плохо
if thing then
  -- ...stuff...
end
function derp()
  -- ...stuff...
end
local wat = 7

-- Хорошо
if thing then
  -- ...stuff...
end

function derp()
  -- ...stuff...
end

local wat = 7

-- Использовать нижнее подчеркиевание для игнорирования переменных в циклах
-- Хорошо
for _, name in pairs(names) do
  -- ...stuff...
end

-- Название объектов
-- Плохо
local OBJEcttsssss = {}
local thisIsMyObject = {}
local this-is-my-object = {}

local c = function()
  -- ...stuff...
end

-- Хорошо
local this_is_my_object = {}

local function do_that_thing()
  -- ...stuff...
end

-- Фабрики
-- Плохо
local player = require('player')

-- Хорошо
local Player = require('player')
local me = Player({ name = 'Jack' })

-- Избавляемся от unpuck
local a = { 100, 200, 300, 400 }
 
for i = 1,100 do
    print( a[1],a[2],a[3],a[4] )
end

-- Умножение более рекомендуемо
-- Хорошо
x * 0.5
y * 0.125
-- Плохо
x / 2
y / 8


-- Предзагрузка звуков
local soundTable = {
  mySound1 = audio.loadSound( "a.wav" ),
  mySound2 = audio.loadSound( "b.wav" ),
  mySound3 = audio.loadSound( "c.wav" ),
  mySound4 = audio.loadSound( "d.wav" ),
  mySound5 = audio.loadSound( "e.wav" ),
  mySound6 = audio.loadSound( "f.wav" ),
  mySound7 = audio.loadSound( "g.wav" ),
  mySound8 = audio.loadSound( "h.wav" ),
}

local mySound = audio.play( soundTable["mySound1"] )

--Очистка звуковой базы
for s = #soundTable,1,-1 do
  audio.dispose( soundTable[s] ) ; soundTable[s] = nil
end