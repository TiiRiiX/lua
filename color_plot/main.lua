--[[
========================
=====OOOOOO=O=====O=====
=====O======OO====O=====
=====O======O=O===O=====
=====OOO====O=====O=====
=====O======O===O=O=====
=====O======O====OO=====
=====O======O=====O=====
========================
=====OOOOOOOOOOOOOO=====
========================
========PSUMAP==========
====COPYRIGHT FNIGHT====
========================
]]--

local composer = require("composer")
local json = require("json")
local scene = composer.newScene()


baseTable = {}

FILE_PATH = system.pathForFile("base.json", system.DocumentsDirectory)


function loadBase()
	local file = io.open(FILE_PATH, "r")

	if file then
		local contents = file:read("*a")
		io.close(file)
		baseTable = json.decode(contents)
	end

--инициализация базы
	if (not(baseTable.history_search)) then
		baseTable = {
		history_search = {},
		}
	end
end

function saveBase()
	for i = #baseTable, 11, -1 do
        table.remove( baseTable, i )
    end

    local file = io.open( FILE_PATH, "w" )

    if file then
        file:write( json.encode( baseTable ) )
        io.close( file )
    end
end

loadBase()

--Сохраняем процесс музыки на iOS
if (system.getInfo("platformName") == "iPhone OS") then
	local otherAudioIsPlaying = false   
	if (audio.supportsSessionProperty) then
	    audio.setSessionProperty(audio.MixMode, audio.AmbientMixMode)
	    if (audio.getSessionProperty(audio.OtherAudioIsPlaying) ~= 0) then
	        otherAudioIsPlaying = true
	    end
	end
end


function gotoScene(set_scene,set_effect)
	composer.removeScene(set_scene)
	composer.gotoScene(set_scene, {time = 300, effect=set_effect})
end

function goExit()
	native.requestExit()
end


local function onKeyEvent(event)
		if(event.phase == "up" and event.keyName == "back") then
			local curScene = composer.getSceneName( "current" )
			if (curScene == "scene.shop") then
				gotoScene("scene.mainMenu", "fromBottom")
			elseif (curScene == "scene.game") then
				gotoScene("scene.mainMenu", "fromBottom")
			elseif (curScene == "scene.mainMenu") then
				native.requestExit()
			else print("Это не работает") 
			end
			return true
		else 
			return false
		end
		
	end

Runtime:addEventListener("key", onKeyEvent)


function goGame()
	local function deleteSplash()
		display.remove(splash)
	end
	composer.gotoScene("scenes.mainScreen", { time=500, effect="crossFade" } )
	transition.to(splash,{time = 500, alpha = 0, onComplete = deleteSplash})
end

splash = display.newImageRect("images/splash.png", 300, 300)
splash.x, splash.y = display.contentCenterX, display.contentCenterY
splash.alpha = 0
if (system.getInfo( "environment" ) == "simulator") then
	transition.to(splash,{time = 0, alpha = 1, onComplete = goGame})
else 
	transition.to(splash,{time = 2500, alpha = 1, onComplete = goGame})
end

