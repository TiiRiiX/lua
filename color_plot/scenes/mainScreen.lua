local composer = require("composer")
local scene = composer.newScene()

function scene:create( event )

  local sceneGroup = self.view

  local ACH = display.actualContentHeight
  local ACW = display.actualContentWidth
  local SOX = display.screenOriginX
  local CCX = display.contentCenterX
  local CCY = display.contentCenterY

  --[[
  local colors = {}
  local positions = {}

  for i = 1, 36 do
    if i <= 18 then
      local H = i * 17
      local S = math.random( 80, 100 )
      local V = math.random( 80, 100 )
      local Hi = math.fmod( math.floor( H / 60 ), 6 )
      local Vmin = ( ( 100 - S ) * V ) / 100
      local a = ( V - Vmin ) * ( math.fmod( H, 60 )/ 60 )
      local Vinc = Vmin + a
      local Vdec = V - a
      local R, G, B
      if Hi == 0 then
        R, G, B = V, Vinc, Vmin
      elseif Hi == 1 then
        R, G, B = Vdec, V, Vmin
      elseif Hi == 2 then
        R, G, B = Vmin, V, Vinc
      elseif Hi == 3 then
        R, G, B = Vmin, Vdec, V
      elseif Hi == 4 then
        R, G, B = Vinc, Vmin, V
      else
        R, G, B = V, Vmin, Vdec
      end
      colors[i] = {R / 100, G / 100, B / 100}
    end
    positions[i] = i
  end

  for i = 36, 1, -1 do
    local j = math.random( i + 1 )
    positions[i], positions[j] = positions[j], positions[i]
  end

  local pool_group = display.newGroup()
  sceneGroup:insert( pool_group )

 
  local opened_tile = {}


  local function openOneTile( object )
    local x, y = object:localToContent( 0, 0 )
    local temp_tile = display.newRoundedRect(
        x,
        y,
        object.width,
        object.height,
        10
      )
      temp_tile:setFillColor( 0.8 )
      transition.to( temp_tile, {
        alpha = 0, 
        time = 150,
        onComplete = function() 
          temp_tile:removeSelf()
        end
      })
      object:removeEventListener( "tap", clickTile )
      object:setFillColor( unpack( object.color ) )
  end


  local function closeOneTile( object )
    local x, y = object:localToContent( 0, 0 )
    local temp_tile = display.newRoundedRect(
      x,
      y,
      object.width,
      object.height,
      10
    )
    temp_tile:setFillColor( unpack( object.color ) )
    object:setFillColor( 0.8 )
    object:addEventListener( "tap", clickTile )
    transition.to( temp_tile, {
      alpha = 0, 
      time = 150,
      onComplete = function()
        temp_tile:removeSelf()
      end
    })
  end


  local function changeTile()
    if #opened_tile == 2 then
      if opened_tile[1].color == opened_tile[2].color then
        transition.to( opened_tile[1], {
          alpha = 0,
          time = 200,
        })
        transition.to( opened_tile[2], {
          alpha = 0,
          time = 200,
          onComplete = function()
            for i = 1, 2 do
              opened_tile[#opened_tile]:removeSelf()
            end
            opened_tile = {}
          end
          })
      else
        for i = 1, 2 do
          closeOneTile( opened_tile[i] )
        end
        opened_tile = {}
      end
    end
  end


  function clickTile( event )
    if #opened_tile < 2 then
      openOneTile(event.target)
      opened_tile[#opened_tile + 1] = event.target
    end
    timer.performWithDelay( 500, changeTile, 1 )
  end


  local p = 1

  for i = 0,5 do
    for j = 0,5 do
      pool_group:insert(
        display.newRoundedRect(
          j * ( ACW / 8 + 20 ),
          i * ( ACW / 8 + 20 ),
          ACW / 8,
          ACW / 8,
          10
        )
      )
      pool_group[pool_group.numChildren].color = colors[math.ceil( positions[p]/2 )]
      pool_group[pool_group.numChildren]:setFillColor( 
        unpack( pool_group[pool_group.numChildren].color )
      )
      p = p + 1
    end
  end

  pool_group.x = 90 + SOX / 1.6
  pool_group.y = CCY / 1.5

  local function startClose()
    local t = 1
    local function startCloseWithTime()
      closeOneTile(pool_group[positions[t])
      t = t + 1
    end
    for i = 1, pool_group.numChildren do
      timer.performWithDelay( 50*i, startCloseWithTime, 1 )
    end
  end

  timer.performWithDelay( 5000, startClose, 1 )
  ]]



  local runtime = 0
   
  local function getDeltaTime()
      local temp = system.getTimer()  -- Get current game time in ms
      local dt = (temp-runtime) / (1000/60)  -- 60 fps or 30 fps as base
      runtime = temp  -- Store game time
      return dt
  end

  local counter = display.newText("0", CCX, 200, native.systemFont)

  local fps = display.newText("30", CCX, 300, native.systemFont)


  local group = display.newGroup()

  local function creteRect()
    for i = 1, 1000 do
      group:insert(display.newRect(CCX, CCY, 200, 200))
      --group[group.numChildren].isVisible = false
      counter.text = tostring(tonumber(counter.text) + 1)
      fps.text = tostring(getDeltaTime()*60)
    end
  end

  timer.performWithDelay(1, creteRect, 0)

end

function scene:destroy(event)
  local sceneGroup = self.view
  display.remove(sceneGroup)
  sceneGroup = nil
end

scene:addEventListener("create", scene)
scene:addEventListener("destroy", scene)

return scene
